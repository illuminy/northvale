# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------
import src.room as room
import src.event as event
import src.player as player
import src.action as action
import src.item as item
import textwrap
import pickle
import os


# -----------------------------------------------------------------------------
SAVED_PATH = "saved_games"
DATA_PATH = "data"


# -----------------------------------------------------------------------------
class TextColor:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# -----------------------------------------------------------------------------
def main():
    """The main function"""
    p = None
    ridx = 0
    in_game = True

    while in_game:
        # Menu
        while True:
            print(TextColor.HEADER)
            print("\nWelcome to Northvale!")
            print('{:=<25}'.format(""))
            print(TextColor.UNDERLINE + TextColor.BOLD)
            print("Main Menu")
            print(TextColor.ENDC + TextColor.HEADER)
            start = input("(N)ew Game, (L)oad Game or (Q)uit: ")
            if start.lower() in ["n", "new", "new game"]:
                name = input("Enter character name: ")
                print(
                    "\nYou have entered the realm of Northvale adventurer!"
                    "\nYou find yourself in the medieval town of Stathmore")
                print('{:=<65}'.format(""))
                break

            elif start.lower() in ["l", "load", "load game"]:
                p, ridx = load_game()
                if p is not None:
                    print("\n{}'s game has been loaded".format(p.get_name()))
                    break

            elif start.lower() in ["q", "quit", "exit"]:
                quit()

            else:
                print("Unknown Command")
        print(TextColor.ENDC)

        # Handles the rooms
        rc = room.RoomControl(os.path.join(DATA_PATH, "roomdata.json"))

        # Creates a player
        if p is None:
            p = player.Player(action.Action(), name)

        # Handles events
        ec = event.EventControl(os.path.join(DATA_PATH, "eventdata.json"))

        # Handles items
        ic = item.ItemControl(os.path.join(DATA_PATH, "itemdata.json"))

        # Starts the game loop
        in_game = game_loop(rc, p, ec, ic, ridx)
        p = None


# -----------------------------------------------------------------------------
def game_loop(rc, p, ec, ic, ridx):
    """List of all the commands"""
    cmdlist = [

        ["To exit game type:", "exit", "quit", "q"],
        ["For help type:", "help", "h", "?"],
        ["To go north type:", "go north", "north", "n"],
        ["To go south type:", "go south", "south", "s"],
        ["To go west type:", "go west", "west", "w"],
        ["To go east type:", "go east", "east", "e"],
        ["To go up type:", "go up", "up"],
        ["To go down type:", "go down", "down"],
        ["For info type:", "info", "i"],
        ["To use aid type:", "use aid", "aid"],
        ["To save game type:", "save game", "save"]

    ]

    # Informs the player about his/hers stats etc
    print("Your name is {0}, you have {1} hp and {2} gold. Good luck on your adventures!"
          .format(p.get_name(), p.get_health(), p.get_currency(), p.get_inventory()))

    # Retrives rooms
    r = rc.get_room(ridx)
    print("\n{0} \n".format(r.get_name()))
    idx = None

    helpinf = "| Type '?', 'h' or 'help' for help/list of common commands |"
    print("".rjust(len(helpinf), '-'))
    print(helpinf)
    print("".rjust(len(helpinf), '-'))
    print("\n")

    # Command loop
    while True:
        cmd = input(TextColor.OKBLUE + "Cmd >> ")
        print(TextColor.ENDC)
        # Handles the users commands
        if cmd.lower() in cmdlist[0]:
            return False

        elif cmd.lower() in cmdlist[1]:
            helpcmd(cmdlist)

        elif cmd.lower() in cmdlist[2]:
            print("You go north")
            idx = move("n", r)

        elif cmd.lower() in cmdlist[3]:
            print("You go south")
            idx = move("s", r)

        elif cmd.lower() in cmdlist[4]:
            print("You go west")
            idx = move("w", r)

        elif cmd.lower() in cmdlist[5]:
            print("You go east")
            idx = move("e", r)

        elif cmd.lower() in cmdlist[6]:
            print("You go up")
            idx = move("up", r)

        elif cmd.lower() in cmdlist[7]:
            print("You go down")
            idx = move("down", r)

        elif cmd.lower() in cmdlist[8]:
            info(p, r)

        elif cmd.lower() in cmdlist[9]:
            update_health(p)

        elif cmd.lower() in cmdlist[10]:
            save_game(p, r)

        # Changes room if it exists
        if idx is not None:
            r = rc.get_room(idx)
            for tex in textwrap.wrap(r.get_player_info(), 80):
                print(tex)

        # Checks event and affects the player
        if idx is not None:
            e = ec.get_event()
            t = e.get_probability_effect()
            if check_status(p.event_effect(t)):
                return True
            i = ic.get_item()
            # adds(or doesnt add) the item to the players inventory
            if i.recive_item() != 0 and i not in p.get_inventory():
                add_item(p, i)
                print(TextColor.OKGREEN)
                print("\nYou've found a(n) {}".format(i.get_name()))
                print(TextColor.ENDC)
            if t != 0:
                print(TextColor.WARNING)
                print(
                    "You {0}! You gained/lost {1} hp, your current health is {2}\n".format(e.get_event(),
                                                                                           e.get_effect(),
                                                                                           p.get_health()))

            if t < 0:
                respons = input("Do you want to (Run) or (Fight)?: ")
                if respons.lower() in ["fight", "f"]:
                    update_health(p)
                    weapon_effect = select_weapon(p)
                    is_hero, is_dead = fight(p, e)
                    if is_dead:
                        return is_dead
                    if weapon_effect:
                        p.get_action().set_effect(p.get_action().get_effect() / weapon_effect)
                    if is_hero is False:
                        print("You've fled from the fight")
                        p.set_health(p.get_health() + e.get_effect())

                else:
                    p.get_action().run(t)
                    if check_status(p.event_effect(e.get_effect())):
                        return True

                    print("\nYou fled. Your have {0} hp remaining after the fight".format(p.get_health()))
            print(TextColor.ENDC)

            idx = None


# -----------------------------------------------------------------------------
def move(direction, r):
    """Controls the players movement"""
    if direction in r.get_neighbours():
        return r.get_neighbours()[direction]

    else:
        print("You can't go there!\n")
        return None


# -----------------------------------------------------------------------------
def load_game():
    """Handles the loading of a saved game"""
    print("Currently saved files/games:")
    if not os.path.exists(SAVED_PATH):
        print("{}".format("Can't find any saved games"))
        return None, 0
    print(os.listdir(SAVED_PATH))

    while True:
        which_load = input("\nTo load game insert ('<filename>.pkl'): ")
        which_load = os.path.join(SAVED_PATH, which_load)
        if os.path.isfile(which_load):
            with open(which_load, "rb") as clo:
                saved_data = pickle.load(clo)
            p = saved_data
            r = saved_data.get_room()
            return p, r
        elif which_load == "":
            return None, 0
        else:
            print("File name not found. Press enter to return to main menu")


# -----------------------------------------------------------------------------
def save_game(p, r):
    """Handles the saving of a game"""
    if not os.path.exists(SAVED_PATH):
        os.makedirs(SAVED_PATH)

    p.set_room(r.get_idx())
    ask_name = input("Save game under file name(<filename>.pkl): ")
    path_name = os.path.join(SAVED_PATH, ask_name)
    with open(path_name, "wb") as klo:
        pickle.dump(p, klo)
    print("Game saved to {}\n".format(ask_name))


# -----------------------------------------------------------------------------
def add_item(p, i):
    """Adds item to the inventory if it doesnt already have one"""
    if p.has_inventory(i):
        return False
    p.add_inventory(i)
    return True


# -----------------------------------------------------------------------------
def helpcmd(h):
    """Help command for the player"""
    for i in h:
        s = i[0]
        for j in i[1:]:
            if j == i[-1]:
                s = s + " or " + j
            elif j == i[1]:
                s = s + "\n" + j
            else:
                s = s + ", " + j
        print("{}\n".format(s))


# -----------------------------------------------------------------------------
def info(p, r):
    """Information command for the player"""
    print("You are currently at {0}. Your current health is {1}. You have {2} gold and you have {3} in your inventory\n"
          .format(r.get_name(), p.get_health(), p.get_currency(), p.get_inventory()))


# -----------------------------------------------------------------------------
def fight(p, e):
    """Confrontation loop"""
    while True:
        alive = e.attacked(p.get_action().kill())
        if check_status(p.event_effect(e.get_effect())):
            return True, True

        if alive is False:
            print("\nThe enemy has been slain! You have {0} hp remaining after the battle".format(p.get_health()))
            return True, False
        respons = input(
            "\nThe enemy has {0} health. Your health is {1}. Do you wish to continue attacking?(Yes/No): ".format(
                e.get_health(), p.get_health()))
        update_health(p)
        if respons.lower() in ["yes", "y"]:
            continue
        elif respons.lower() in ["no", "n"]:
            return False, False
        else:
            print("\nUnknown/unable command. Try again\n")
            respons = input(
                "The enemy has {0} health. Your health is {1}. Do you wish to continue attacking?(Yes/No): ".format(
                    e.get_health(),
                    p.get_health()))
            if respons.lower() in ["yes", "y"]:
                continue
            elif respons.lower() in ["no", "n"]:
                return False, False
            else:
                p.set_health(0)
                print(TextColor.ENDC + TextColor.BOLD + TextColor.UNDERLINE)
                print("Because of your incompetence to make a decision the enemy has gotten the better of you")
                print("You've died.\n")
                print(TextColor.ENDC)
                return True, True


# -----------------------------------------------------------------------------
def update_health(p):
    """Use health items"""
    for i in p.get_inventory():
        if "aid" == i.get_type():
            aid_use = input("\nDo you wish to use {0} which will bring your health up to {1}?(Yes/No) "
                            .format(i.get_name(), p.get_health() + i.get_effect()))
            if aid_use.lower() in ["yes", "y"]:
                p.set_health(p.get_health() + i.get_effect())
                print("Your current health is {}".format(p.get_health()))
                p.remove_inventory(i)
            elif aid_use.lower() in ["no", "n"]:
                print("Hope you survive")


# -----------------------------------------------------------------------------
def select_weapon(p):
    """Equips or doesnt equip weapon during combat"""
    for i in p.get_inventory():
        if "weapon" == i.get_type():
            weapon_equip = \
                input("\nDo you wish to equip the {0}, you will deal {1} damage while using the {0}?(Yes/No) "
                      .format(i.get_name(), p.get_action().get_effect() * i.get_effect()))
            if weapon_equip.lower() in ["yes", "y"]:
                p.get_action().set_effect(p.get_action().get_effect() * i.get_effect())
                print("Your currently dealing {0} while using the {1}".format(p.get_action().get_effect(),
                                                                              i.get_name()))
                p.remove_inventory(i)
                return i.get_effect()
            elif weapon_equip.lower() in ["no", "n"]:
                print("I hope it's not a dragon you're fighting")
    return None


# -----------------------------------------------------------------------------
def check_status(is_dead):
    if is_dead:
        print(TextColor.ENDC + TextColor.BOLD + TextColor.UNDERLINE)
        print("Something struck from the shadows!")
        print("You've died\n")
        print(TextColor.ENDC)
        return is_dead


if __name__ == '__main__':
    main()
