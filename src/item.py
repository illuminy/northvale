# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------
import json
import random


# -----------------------------------------------------------------------------
class Item:
    def __init__(self, idx=0, name="", probability=0, description="", effect=0, type=""):
        self._idx = idx
        self._name = name
        self._probability = probability
        self._description = description
        self._effect = effect
        self._type = type

    def get_name(self):
        return self._name

    def get_effect(self):
        return self._effect

    def get_type(self):
        return self._type

    # Chance of reciveing an item
    def recive_item(self):
        if self._probability >= random.random():
            return self._effect
        return 0

    # Prints name instead of memory id
    def __repr__(self):
        return "{}".format(self._name)

    # Compares name instead of memory id
    def __eq__(self, other):
        return self._name == other.get_name()


# -----------------------------------------------------------------------------
class ItemControl:
    """The existing item table"""
    def __init__(self, jfile):
        self._jfile = jfile
        self._data = None

    def get_item(self):
        if self._data is None:
            self.__read_data()
        # Shuffle around all dictionaries in the list
        random.shuffle(self._data)
        # Return a new Event
        return Item(**self._data[0])

    def __read_data(self):
        # Open the jason file
        with open(self._jfile, "r") as fileopen:
            d = fileopen.read()
        # Store data as list of dictionaries
        # [{},{},{}{}]
        self._data = json.loads(d)
