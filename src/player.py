# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------


class Player:
    """Represents the player in the game"""
    def __init__(self, action, name, health=250, currency=400):
        self._action = action
        self._name = name
        self._health = health
        self._currency = currency
        self._inventory = []
        self._sridx = None

    def get_action(self):
        return self._action

    def get_name(self):
        return self._name

    def get_health(self):
        return self._health

    def get_currency(self):
        return self._currency

    def get_inventory(self):
        return self._inventory

    def set_health(self, healed):
        self._health = healed

    def add_inventory(self, inventory):
        self._inventory.append(inventory)

    def remove_inventory(self, inventory):
        self._inventory.remove(inventory)

    def event_effect(self, effect):
        self._health += effect
        return self._isdead()

    def has_inventory(self, inventory):
        if inventory in self._inventory:
            return True
        return False

    def _isdead(self):
        if self._health <= 0:
            return True
        return False

    def __repr__(self):
        return "{}, {}, {}, {}, {}".format(self._action, self._name, self._health, self._currency, self._inventory)

    def set_room(self, sridx):
        self._sridx = sridx

    def get_room(self):
        return self._sridx
