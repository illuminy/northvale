# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------
import json
import random


# -----------------------------------------------------------------------------
class Event:
    def __init__(self, event="", probability=0.0, occurrences=0, effect=0, health=0):
        self._event = event
        self._probability = probability
        self._occurrences = occurrences
        self._effect = effect
        self._health = health

    def get_event(self):
        return self._event

    def get_effect(self):
        return self._effect

    def get_health(self):
        return self._health

    def attacked(self, value):
        self._health += value
        if self._health <= 0:
            return False
        return True

    def effect_again(self):
        return self._effect

    def get_probability_effect(self):
        if self._probability >= random.random():
            return self._effect
        return 0


# -----------------------------------------------------------------------------
class EventControl:
    def __init__(self, jfile):
        self._jfile = jfile
        self._data = None

    def get_event(self):
        if self._data is None:
            self.__read_data()
        # Shuffle around all dictionaries in the list
        random.shuffle(self._data)
        # Return a new Event
        return Event(**self._data[0])

    def __read_data(self):
        # Open the jason file
        with open(self._jfile, "r") as fileopen:
            d = fileopen.read()
            # Store data as list of dictionaries
            # [{},{},{}{}]
        self._data = json.loads(d)
