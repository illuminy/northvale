# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------


class Action:
    """The possible actions the player has in combat"""
    def __init__(self, effect=-10, actionname="hit"):
        self._effect = effect
        self._actionname = actionname

    def get_effect(self):
        return self._effect

    def set_effect(self, weapon_use):
        self._effect = weapon_use

    def kill(self):
        return self._effect

    def run(self, v):
        return v

    def __repr__(self):
        return "{}, {}".format(self._effect, self._actionname)
