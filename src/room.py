# -----------------------------------------------------------------------------
#   Carl Larsson Na16D
#   carl.larsson@live.vasteras.se
#   Text adventure game
# -----------------------------------------------------------------------------
import json


# -----------------------------------------------------------------------------
class Room:
    def __init__(self, idx=0, name="", description="", player_info="", neighbours=None):
        self._idx = idx
        self._name = name
        self._description = description
        self._player_info = player_info
        self._neighbours = neighbours

    def get_idx(self):
        return self._idx

    def get_name(self):
        return self._name

    def get_player_info(self):
        return self._player_info

    def get_neighbours(self):
        return self._neighbours


# -----------------------------------------------------------------------------
class RoomControl:
    def __init__(self, jfile):
        self._jfile = jfile
        self._data = None

    def get_room(self, idx):
        if not self._data:
            self.__read_data()
        # Room(self.data[idx]['idx'],self.data[idx]['name'], self.data[idx]['description'],
        # self.data[idx]['player_info'], self.data[idx]['neighbours'])
        return Room(**self._data[idx])

    def __read_data(self):
        # Open the jason file
        with open(self._jfile, "r") as fileopen:
            d = fileopen.read()
        # Store data as list of dictionaries
        # [{},{},{}{}]
        self._data = json.loads(d)
