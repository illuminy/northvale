### Bra
Effektiv använding av kod.

Dåligt:
Fortfarande i utvecklings stadie. kommentarer saknas ibland, eller är dåliga.

Vad som finns:
De mesta basic för ett text adventure.

Vad som Fattas:
Inventory är inte implementerat.
Item är inte implementerat.
Jag ville också ha events som bara inträffade vid vissa platser men pga tidsbrist så var jag tvungen och fokusera på annat.

Förbättringar:
Utveckla de berättande texterna och förbättra spelar interaktionen.